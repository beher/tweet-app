

var app = new Vue({

  el: '#app',
  data: {
    showModal: false,
    tweet: '',
    tweets: [],
    newTweet: '',
      minChars: 5,
    maxChars: 140,
    hasMaxChars: false,
      hasMinChars: true,
    loading: false
  },
  computed: {},

    methods: {
        isLoading: function(show) {

            this.loading=show
            setTimeout(()=>{
                this.loading=false;
            this.closeModal();
        },1000);
        },

        addTweet: function() {
            this.tweets.unshift(this.tweet),
             this.isLoading(true)

        },

        openModal: function() {

            this.showModal= true,
                console.log(this.showModal)
        },

        closeModal: function() {

            this.showModal= false,
                this.tweet=''
        },

        checkChars: function() {
            if(this.tweet.length > this.maxChars) {
                this.hasMaxChars = true
            }
            else if (this.tweet.length < this.minChars) {
                this.hasMinChars = true
            }
            else {
                this.hasMaxChars = false
                this.hasMinChars = false
            }
        }
    }
});
